import { createStackNavigator, createAppContainer } from 'react-navigation'
import Home from '../screens/Home'
import Main from '../screens/Main'
import Login from '../screens/Login'
import SignUp from '../screens/SignUp'
import Chat from '../screens/Chat'
import ScanCode from '../screens/ScanCode'

const AppNavigator = createStackNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      header: null
    }
  },
  Main: {
    screen: Main,
    navigationOptions: {
      header: null
    }
  },
  Login: {
    screen: Login,
    navigationOptions: {
      header: null
    }
  },
  SignUp: {
    screen: SignUp,
    navigationOptions: {
      header: null
    }
  },
  Chat: {
    screen: Chat
  },
  ScanCode: {
    screen: ScanCode,
    navigationOptions: {
      header: null
    }
  }
});

export default createAppContainer(AppNavigator);
