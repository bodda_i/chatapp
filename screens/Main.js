import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet, FlatList, ActivityIndicator, Dimensions } from 'react-native';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import { GiftedChat } from 'react-native-gifted-chat';
import QRCode from 'react-native-qrcode';
import { signOut, isSigned } from '../helpers/AuthUser'
import fire from '../helpers/Fire';

// const rows = [
//   {userId: 'vSUTdrVqIJYvgoroiiIDj9kMJtf1', userName: 'Second Bla'}
// ]

const extractKey = ({userId}) => userId

class HomeScreen extends React.Component<Props> {
  constructor(props) {
    super(props);
  }

  state = {
    messages: [],
    showProgress: 0
  };

  chatPressed(id, name) {
    this.props.navigation.push('Chat', {
      id: id,
      name: name
    });
  }

  renderItem = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => this.chatPressed(item.userId, item.userName)}>
        <Text style={styles.row}>
          {item.userName}
        </Text>
      </TouchableOpacity>
    )
  }

  render() {
    return (
      <View style={styles.containerList}>
        <ActivityIndicator size="large"
          style={[styles.spinner, {zIndex: this.state.showProgress}, {opacity: this.state.showProgress}]}
          animating={true} />
        <FlatList
          style={styles.containerHome}
          data={this.state.messages}
          renderItem={this.renderItem}
          keyExtractor={extractKey}
        />
      </View>
    );
  }

  componentDidMount() {
    this.setState({showProgress: 1});
    isSigned().then (res => {
      if(res == false) {
        this.props.navigation.replace('Login');
      }
      else {
        fire.getRefFriends(JSON.parse(res).userId).on('value', snapshot => {
          this.setState({showProgress: 0});
          if(snapshot.val() != null)
          {
            var allMessages = [];
            for(var propName in snapshot.val()) {
                if(snapshot.val().hasOwnProperty(propName)) {
                    var propValue = snapshot.val()[propName];
                    const { id, name } = propValue;
                    const message = {
                      userId: id,
                      userName: name
                    };
                    allMessages.push(message);
                }
            }
            this.setState({messages: allMessages});
          }
        });
      }
    })
  }

  componentWillUnmount() {
    fire.refOffFriends();
  }
}

class UserScreen extends React.Component {
  state = {
    userQrCode: ""
  };

  scanCodePressed() {
    this.props.navigation.push("ScanCode");
  }

  render() {
    return (
      <View style={styles.container}>
        <QRCode
          value={this.state.userQrCode}
          size={200}
          bgColor='black'
          fgColor='white'/>
          <TouchableOpacity
            style={[styles.loginButton, styles.scanCodeButton]}
            onPress={() => this.scanCodePressed()}>
            <Text style={styles.buttonText}>SCAN CODE</Text>
          </TouchableOpacity>
      </View>
    );
  }

  componentWillMount() {
    isSigned().then (res => {
      if(res == false) {
        this.props.navigation.replace('Login');
      }
      else {
        var userObj = JSON.parse(res)
        var userId = userObj.userId;
        var userName = userObj.userName;
        var userQrCode = userId.concat("-", userName);
        this.setState({userQrCode: userQrCode})
      }
    })
  }
}

class LogOutScreen extends React.Component {
  logoutPressed() {
    signOut();
    fire.onLogout();
    this.props.navigation.replace("Login");
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.loginButton}
          onPress={() => this.logoutPressed()}>
          <Text style={styles.buttonText}>LOGOUT</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

// const TabNavigator = createBottomTabNavigator({
//   Home: HomeScreen,
//   User: UserScreen,
//   LogOut: LogOutScreen,
// });

export default createBottomTabNavigator(
  {
    Home: HomeScreen,
    User: UserScreen,
    LogOut: LogOutScreen,
  },
  {
    tabBarOptions: {
      activeTintColor: '#129793',
      inactiveTintColor: 'gray',
      tabStyle: {
        padding: 32
      }
    },
  }
);

const styles = StyleSheet.create ({
  containerHome: {
    marginTop: 20,
    flex: 1,
  },

  containerList: {
    flex: 1
  },

  row: {
    padding: 15,
    marginBottom: 5,
    backgroundColor: 'white',
    color: '#129793',
    borderWidth: 1,
    borderColor: '#129793'
  },

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },

  loginButton: {
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: '#129793',
    borderRadius: 30,
    alignItems: 'center',
    width: 100
  },

  scanCodeButton: {
    marginTop: 50
  },

  buttonText: {
    color: 'white',
    fontSize: 15
  },

  spinner: {
    position: 'absolute',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
    backgroundColor: 'rgba(245, 246, 247, 0.7)'
  }
})
