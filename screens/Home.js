import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { isSigned } from '../helpers/AuthUser'

export default class Home extends React.Component {
  componentWillMount() {
    isSigned().then (res => {
      if(res == false) {
        this.props.navigation.replace('Login');
      }
      else {
        this.props.navigation.replace('Main');
      }
    })
  }

  render() {
    return (
      <View style={styles.container}>
      </View>
    )
  }
}

const styles = StyleSheet.create ({
  container: {
    backgroundColor: '#129793',
    flex: 1
  }
})
