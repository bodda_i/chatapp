import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import fire from '../helpers/Fire';
import { isSigned } from '../helpers/AuthUser'


export default class ScanCode extends React.Component {
  onSuccess = (e) => {
    isSigned().then (res => {
      if(res == false) {
        this.props.navigation.replace('Login');
      }
      else {
        var userObj = JSON.parse(res)
        var userId = userObj.userId;
        var userName = userObj.userName;
        var array = e.data.split("-");
        fire.sendQrCode(userId, userName, array[0], array[1]);
        this.props.navigation.push("Main");
      }
    })
  }

  render() {
    return (
      <QRCodeScanner
        onRead={this.onSuccess}
        topContent={
          <Text style={styles.centerText}>
            Scan your friend's QR code
          </Text>
        }
        bottomContent={
          <TouchableOpacity style={styles.buttonTouchable}>
            <Text style={styles.buttonText}>OK. Got it</Text>
          </TouchableOpacity>
        }
      />
    );
  }
}

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
});
