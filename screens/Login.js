import React from 'react'
import { Text, View, Image, TextInput, Button, TouchableOpacity, TouchableWithoutFeedback, ActivityIndicator, StyleSheet, Dimensions, Keyboard } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import firebase from 'firebase';
import { auth, initializeApp, storage } from 'firebase';
import uuid from 'uuid';
import NetInfo from '@react-native-community/netinfo';
import { changeEmail } from '../js/actions/EmailAction';
import { changePass } from '../js/actions/PassAction';
import { changeLoader } from '../js/actions/LoaderAction';
import fire from '../helpers/Fire';
import { signIn } from '../helpers/AuthUser'

class Login extends React.Component {
  loginPressed() {
    var email = this.props.mainR.email;
    var password = this.props.mainR.password;
    if(!email) {
      alert("Email can't be empty");
      return;
    }
    if(!password) {
      alert("Password can't be empty");
      return;
    }
    NetInfo.isConnected.fetch().then(isConnected => {
      if(isConnected)
       {
        this.props.changeLoader(1);
        const user = {
          email: email,
          password: password
        };
        fire.login(user).then (res => {
          this.props.changeLoader(0);
          if(res !== null) {
            var userObj = res.user;
            signIn(userObj.uid, userObj.displayName);
            this.props.navigation.replace('Main');
          }
          else {
            this.props.changeLoader(0);
            alert("Error happened...Try again later");
          }
        })
        .catch(err => {
          this.props.changeLoader(0);
           alert("Wrong Credentials");
         });
        }
        else {
         alert("You are offline");
       }
     })
   }

   signUpPressed() {
     this.props.navigation.push('SignUp');
   }

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
          <ActivityIndicator size="large"
            style={[styles.spinner, {zIndex: this.props.mainR.showProgress}, {opacity: this.props.mainR.showProgress}]}
            animating={true} />
          <Image style={styles.logo} source={require('../resources/images/logo.png')} />
          <Text style={styles.formText}>WELCOME</Text>
          <View style={styles.formContainer}>
            <TextInput style={styles.inputField} placeholder="Email" onChangeText={(text) => this.props.changeEmail(text)} />
            <TextInput style={[styles.inputField, styles.passwordField]} secureTextEntry={true} placeholder="Password" onChangeText={(text) => this.props.changePass(text)} />
            <Text style={styles.forgotPasswordLink}>Forgot Password?</Text>
            <TouchableOpacity
              style={styles.loginButton}
              onPress={() => this.loginPressed()}>
              <Text style={styles.buttonText}>SIGN IN</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.signUpView}>
            <Text style={styles.signUpText}>Don't have an account?</Text>
            <TouchableOpacity
              onPress={() => this.signUpPressed()}>
              <Text style={styles.signUpLink}>Create new account</Text>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const styles = StyleSheet.create ({
  container: {
    backgroundColor: '#F5F6F7',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    paddingBottom: 20
  },

  logo: {
    flexGrow: 1,
    width: 200,
    resizeMode: 'contain',
    alignSelf: 'center'
  },

  formContainer: {
    flexGrow: 1,
    paddingRight: 40,
    paddingLeft: 40
  },

  inputField: {
    borderWidth: StyleSheet.hairlineWidth,
    paddingRight: 20,
    paddingLeft: 20
  },

  passwordField: {
    borderTopWidth: 0
  },

  formText: {
    flexGrow: 1,
    alignSelf: 'center',
    fontSize: 24
  },

  forgotPasswordLink: {
    alignSelf: 'flex-end'
  },

  loginButton: {
    marginTop: 30,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: '#129793',
    borderRadius: 30,
    alignItems: 'center'
  },

  buttonText: {
    color: 'white',
    fontSize: 15
  },

  signUpView: {
    flexGrow: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },

  signUpLink: {
    color: '#E96E6E'
  },

  spinner: {
    position: 'absolute',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
    backgroundColor: 'rgba(245, 246, 247, 0.7)'
  }
})

const mapStateToProps = (state) => {
  const { mainR } = state
  return { mainR }
}

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    changeEmail,
    changePass,
    changeLoader
  }, dispatch)
);

export default connect (mapStateToProps, mapDispatchToProps)(Login)
