import React from 'react';
import { Text, View, TextInput, StyleSheet, ActivityIndicator, Dimensions, TouchableOpacity, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import firebase from 'firebase';
import { auth, initializeApp, storage } from 'firebase';
import uuid from 'uuid';
import NetInfo from '@react-native-community/netinfo';
import { changeFirst } from '../js/actions/FirstAction';
import { changeLast } from '../js/actions/LastAction';
import { changeEmailSign } from '../js/actions/EmailSignAction';
import { changePassSign } from '../js/actions/PassSignAction';
import { changeLoader } from '../js/actions/LoaderAction';
import fire from '../helpers/Fire';


class SignUp extends React.Component {
  validateEmail = (email) => {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  signUpPressed() {
    var firstName = this.props.mainR.firstName;
    var lastName = this.props.mainR.lastName;
    var email = this.props.mainR.emailSign;
    var password = this.props.mainR.passwordSign;
    if(!firstName) {
      alert("First name can't be empty");
      return;
    }
    if(!lastName) {
      alert("Last name can't be empty");
      return;
    }
    if(!email) {
      alert("Email can't be empty");
      return;
    }
    if (!this.validateEmail(email)) {
        alert('Please enter a valid email');
        return;
    }
    if(!password) {
      alert("Password can't be empty");
      return;
    }
    NetInfo.isConnected.fetch().then(isConnected => {
      if(isConnected) {
        this.props.changeLoader(1);
        const user = {
          name: firstName.concat(" ", lastName),
          email: email,
          password: password
        };
        fire.createAccount(user).then (res => {
          this.props.changeLoader(0);
          this.props.navigation.replace("Login");
        })
        .catch((err) => {
          this.props.changeLoader(0);
          alert(err);
         });
      }
      else {
       alert("You are offline");
     }
   })
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
          <ActivityIndicator size="large"
            style={[styles.spinner, {zIndex: this.props.mainR.showProgress}, {opacity: this.props.mainR.showProgress}]}
            animating={true} />
          <View style={styles.formTextContainer}>
            <Text style={styles.formText}>CREATE NEW ACCOUNT</Text>
          </View>
          <View style={styles.formContainer}>
            <View style={styles.formSubContainer}>
              <TextInput style={[styles.inputField, styles.inputFieldName, styles.inputFieldFirstName]} placeholder="First Name" onChangeText={(text) => this.props.changeFirst(text)} />
              <TextInput style={[styles.inputField, styles.inputFieldName]} placeholder="Last Name" onChangeText={(text) => this.props.changeLast(text)} />
            </View>
            <TextInput style={[styles.inputField, styles.inputFieldBorder]} placeholder="Email Address" onChangeText={(text) => this.props.changeEmailSign(text)} />
            <TextInput style={styles.inputField} secureTextEntry={true} placeholder="Password" onChangeText={(text) => this.props.changePassSign(text)} />
          </View>
          <View style={styles.buttonContainer}>
            <Text style={styles.buttonPrimaryText}>By tapping "Sign Up" you agree to the</Text>
            <Text style={styles.buttonSecondaryText}>terms & condition</Text>
            <TouchableOpacity
              style={styles.signUpButton}
              onPress={() => this.signUpPressed()}>
              <Text style={styles.buttonText}>Create new account</Text>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = new StyleSheet.create ({
  container: {
    backgroundColor: '#F5F6F7',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center'
  },

  formText: {
    fontSize: 24
  },

  formTextContainer: {
    flexGrow: 1,
    alignSelf: 'center',
    justifyContent: 'center'
  },

  formContainer: {
    flexGrow: 1,
    paddingRight: 40,
    paddingLeft: 40,
    justifyContent: 'center'
  },

  formSubContainer: {
    flexDirection: 'row'
  },

  inputField: {
    borderWidth: StyleSheet.hairlineWidth,
    paddingRight: 20,
    paddingLeft: 20
  },

  inputFieldName: {
    flexGrow: 1,
    borderBottomWidth: 0
  },

  inputFieldFirstName: {
    borderRightWidth: 0
  },

  inputFieldBorder: {
    borderBottomWidth: 0
  },

  buttonContainer: {
    flexGrow: 1,
    paddingRight: 40,
    paddingLeft: 40
  },

  signUpButton: {
    marginTop: 30,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: '#129793',
    borderRadius: 30,
    alignItems: 'center'
  },

  buttonText: {
    color: 'white',
    fontSize: 15
  },

  buttonPrimaryText: {
    alignSelf: 'center'
  },

  buttonSecondaryText: {
    color: '#E96E6E',
    alignSelf: 'center'
  },

  spinner: {
    position: 'absolute',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
    backgroundColor: 'rgba(245, 246, 247, 0.7)'
  }
})

const mapStateToProps = (state) => {
  const { mainR } = state
  return { mainR }
}

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    changeFirst,
    changeLast,
    changeEmailSign,
    changePassSign,
    changeLoader
  }, dispatch)
);

export default connect (mapStateToProps, mapDispatchToProps)(SignUp)
