import React from 'react';
import { View, BackHandler, StyleSheet, Dimensions, ActivityIndicator  } from 'react-native'
import { GiftedChat } from 'react-native-gifted-chat';
import { isSigned } from '../helpers/AuthUser'
import fire from '../helpers/Fire';

class Chat extends React.Component<Props> {
  constructor(props) {
    super(props);
    //this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  static navigationOptions = ({ navigation }) => ({
    title: (navigation.state.params.name),
  });

  state = {
    messages: [],
    userId: "",
    userName: ""
  };

  get user() {
    return {
      name: this.state.userName,
      receiverId: this.props.navigation.state.params.id,
      senderId: this.state.userId,
      id: this.state.userId,
      _id: this.state.userId,
    };
  }

  render() {
    const { navigation } = this.props;
    const id = navigation.getParam('id');
    const name = navigation.getParam('name');
    return (
        <GiftedChat
          renderLoading={() =>  <ActivityIndicator style={styles.spinner} size="large" color="#129793" />}
          messages={this.state.messages}
          onSend={fire.send}
          user={this.user}
          alwaysShowSend={true}
        />
    );
  }

  componentDidMount() {
    isSigned().then (res => {
      if(res == false) {
        this.props.navigation.replace('Login');
      }
      else {
        var userObj = JSON.parse(res);
        this.setState({userId: userObj.userId});
        this.setState({userName: userObj.userName});
        fire.refOn((message) =>
          {this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, message)
          }))}, JSON.parse(res).userId, this.props.navigation.state.params.id
        );
      }
    })
  }

  componentWillMount() {
    //BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    //BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    fire.refOff();
  }

  // handleBackButtonClick() {
  //   this.props.navigation.pop();
  //   return true;
  // }
}

export default Chat;

const styles = StyleSheet.create ({
  spinner: {
    position: 'absolute',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
    backgroundColor: 'rgba(245, 246, 247, 0.7)'
  }
})
