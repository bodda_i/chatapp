import React from 'react';
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import AppNavigator from './components/AppNavigator';
import mainReducer from './js/reducers/MainReducer'

const store  = createStore(mainReducer)

export default class App extends React.Component {
  render() {
    return (
      <Provider store={ store }>
        <AppNavigator/>
      </Provider>
    )
  }
}
