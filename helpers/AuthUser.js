import AsyncStorage from '@react-native-community/async-storage'

export const USER_KEY = "user_info"

export const isSigned = () => {
  return new Promise ((resolve, reject) => {
    AsyncStorage.getItem(USER_KEY)
    .then(res => {
      if(res !== null) {
        resolve(res)
      }
      else {
        resolve(false)
      }
    })
    .catch(err => reject(err))
  })
}

export const signIn = (id, name) => {
  let userObj = {userId: id, userName: name};
  AsyncStorage.setItem(USER_KEY, JSON.stringify(userObj));
}

export const signOut = () => {
  AsyncStorage.removeItem(USER_KEY);
}
