import firebase from 'firebase';
import uuid from 'uuid';

const config = {
  apiKey: "AIzaSyBbKBZq3OKbwLrq7Vs6E3uD5W1ZAGlbiRM",
  authDomain: "test-chat-app-21e2f.firebaseapp.com",
  databaseURL: "https://test-chat-app-21e2f.firebaseio.com",
  projectId: "test-chat-app-21e2f",
  storageBucket: "test-chat-app-21e2f.appspot.com",
  messagingSenderId: "714330476894",
  appId: "1:714330476894:web:00ddda88503f9c1c"
}

class Fire {
  constructor() {
    if (!firebase.apps.length) {
      firebase.initializeApp(config);
    } else {
      console.log("firebase apps already running...")
    }
  }

  login = (user) => {
    return new Promise ((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(user.email, user.password)
      .then(res => {
        resolve(res)
      })
      .catch(err => reject(err))
    })
  }

  observeAuth = () => firebase.auth().onAuthStateChanged(this.onAuthStateChanged);

  onAuthStateChanged = user => {
    if (!user) {
      try {
        this.login(user);
      } catch ({ message }) {
        console.log("Failed:" + message);
      }
    } else {
      console.log("Reusing auth...");
    }
  };

  createAccount = (user) => {
    return new Promise ((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(user.email, user.password)
      .then((authData) => {
          console.log("User created successfully with payload-", authData);
      })
      .then(() => {
          var userf = firebase.auth().currentUser;
          userf.updateProfile({ displayName: user.name})
      })
      .then(res => {
          resolve(res)
       })
       .catch((err) => {
          console.log("Login Failed!", err);
          reject(err);
      })
    })
  }

  onLogout = user => {
    firebase.auth().signOut().then(function() {
      console.log("Sign-out successful.");
    }).catch(function(error) {
      console.log("An error happened when signing out");
    });
  }

  get uid() {
    return (firebase.auth().currentUser || {}).uid;
  }

  get uName() {
    return (firebase.auth().currentUser || {}).displayName;
  }

  getRef = (senderId, receiverId) => {
    var refId = "";
    if(senderId > receiverId) {
      refId = senderId.concat(receiverId);
    }
    else {
      refId = receiverId.concat(senderId);
    }
    var finalRefId = "Messages".concat("/", refId);
    return firebase.database().ref(finalRefId);
  }

  getRefFriends = (senderId) => {
    var refId = "Friends".concat("/", senderId);
    return firebase.database().ref(refId);
  }

  parse = snapshot => {
    const { timestamp: numberStamp, text, id, _id, name } = snapshot.val();
    const { key } = snapshot;
    user = {
      _id: _id,
      id: id,
      name: name
    }
    const timestamp = new Date(numberStamp);
    const message = {
      user,
      timestamp,
      text,
      _id: key
    };
    return message;
  };

  // parseFriends = snapshot => {
  //   const { id, name } = snapshot.val();
  //   const message = {
  //     userId: id,
  //     userName: name
  //   };
  //   return message;
  // };

  refOn = (callback, senderId, receiverId) => {
    this.getRef(senderId, receiverId)
      .limitToLast(20)
      .on('child_added', snapshot => callback(this.parse(snapshot)));
  }

  // refOnFriends = (callback, senderId) => {
  //   this.getRefFriends(senderId)
  //     .limitToLast(20)
  //     .on('child_added', snapshot => callback(this.parseFriends(snapshot)));
  // }

  // refOnFriends = (senderId) => {
  //   //alert("enter");
  //   this.getRefFriends(senderId).on('value', function (snapshot) {
  //       if(snapshot.val() == null) {
  //         //alert("1");
  //         return null;
  //       }
  //       else {
  //         //alert("2");
  //         return this.parseFriends(snapshot);
  //       }
  //   });
  // }

  get timestamp() {
    return firebase.database.ServerValue.TIMESTAMP;
  }

  send = messages => {
    for (let i = 0; i < messages.length; i++) {
      const { text, user } = messages[i];
      const _id = user._id;
      const id = user.id;
      const receiverId = user.receiverId;
      const senderId = user.senderId;
      const name = user.name;
      const message = {
        text,
        id,
        receiverId,
        senderId,
        _id,
        createdAt: this.timestamp,
        name
      };
      this.getRef(senderId, receiverId).push(message);
    }
  };

  sendQrCode = (userOneId, userOneName, userTwoId, userTwoName) => {
    const messageOne = {
      id: userOneId,
      name: userOneName
    };
    const messageTwo = {
      id: userTwoId,
      name: userTwoName
    };
    this.getRefFriends(userOneId).push(messageTwo);
    this.getRefFriends(userTwoId).push(messageOne);
  };

  refOff() {
    //this.ref.off();
  }

  refOffFriends() {
    //this.ref.off();
  }
}

const fire = new Fire();
export default fire;
