import { combineReducers } from 'redux'

const INITIAL_STATE = {
  showProgress: 0,
  email: "",
  password: "",
  firstName: '',
  lastName: '',
  emailSign: '',
  passwordSign: ''
}

const mainReducer = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case 'CHANGE_EMAIL':
      return Object.assign({}, state, {
        email: action.payload
      })
    case 'CHANGE_PASS':
      return Object.assign({}, state, {
        password: action.payload
      })
    case 'CHANGE_LOADER':
      return Object.assign({}, state, {
        showProgress: action.payload
      })
    case 'CHANGE_FIRST':
      return Object.assign({}, state, {
        firstName: action.payload
      })
    case 'CHANGE_LAST':
      return Object.assign({}, state, {
        lastName: action.payload
      })
    case 'CHANGE_EMAIL_SIGN':
      return Object.assign({}, state, {
        emailSign: action.payload
      })
    case 'CHANGE_PASS_SIGN':
      return Object.assign({}, state, {
        passwordSign: action.payload
      })
    default:
      return state
  }
}

export default combineReducers({
  mainR: mainReducer,
})
