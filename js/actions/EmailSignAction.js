export const changeEmailSign = (email) => ({
  type: 'CHANGE_EMAIL_SIGN',
  payload: email
})
