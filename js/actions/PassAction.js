export const changePass = (pass) => ({
  type: 'CHANGE_PASS',
  payload: pass
})
