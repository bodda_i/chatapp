export const changePassSign = (pass) => ({
  type: 'CHANGE_PASS_SIGN',
  payload: pass
})
