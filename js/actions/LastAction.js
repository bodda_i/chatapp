export const changeLast = (last) => ({
  type: 'CHANGE_LAST',
  payload: last
})
