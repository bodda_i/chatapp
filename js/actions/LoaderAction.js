export const changeLoader = (loader) => ({
  type: 'CHANGE_LOADER',
  payload: loader
})
