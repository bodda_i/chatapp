export const changeEmail = (email) => ({
  type: 'CHANGE_EMAIL',
  payload: email
})
